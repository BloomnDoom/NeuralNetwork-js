const mmap = math.map;
const rand = math.random;
const transp = math.transpose;
const mat = mat.matrix;
const e = math.evaluate;
const sub = math.subtract;
const sqr = math.square;
const sum = math.sum;

class NeuralNetwork
{
    constructor(inputnodes,hiddennodes,outputnodes,learningrate,wih,who)
    {
	this.inputnodes = inputnodes;
	this.hiddennodes = hiddennodes;
	this.outputnodes = outputnodes;
	this.learningrate = learningrate;
	this.wih = wih || sub(mat(rand([hiddennodes,inputnodes]),0.5));
	this.who = who || sub(mat(rand([outputnodes,hiddennodes])),0.5);
	this.act = (matrix) => mmap(matrix, (x) => 1/(1 + Math.exp(-x)));
    }
    cache = { loss: [] };
    static normalizeData = (data) => {/*...*/}
    forward = (input) =>{
	const wih = this.wih;
	const who = this.who;
	const act = this.act;
	input = transp(mat([input]));
	const h_in = e("wih * input",{wih,input});
	const h_out = act(h_in);
	const o_in = e("who * h_out",{who,h_out});
	const actual = act(o_in);
	this.cache.input = input;
	this.cache.ho_out = h_out;
	this.cache.actual = actual;
    };
    backward = (input, target) => {/*...*/};
    update = () => {/*...*/};
    predict = (input) => {/*...*/};
    train = (input,target) => {/*...*/};
}
